#!/usr/bin/env node

'use-strict'

const { doProperAction } = require('./commandDetails');


if (process.argv.length === 2) {
    console.log(
    `Available commands: 
        add <key> <value>
        list
        get <key>
        remove <key>
        clear`
    );
    process.exit();
}

const command = process.argv[2];

doProperAction(command, process.argv.slice(3));






