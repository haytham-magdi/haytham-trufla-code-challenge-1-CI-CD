
const { loadStoreObj, saveStoreObj } = require('./storeIo');

const validateParamsCount = (command, params, requiredCount) => {
    if (params.length !== requiredCount) {
        console.log(command + ' command should have ' + requiredCount + ' parameters');
        process.exit();
    }
}

const commandActions = {

    add: (params) => {
        validateParamsCount('add', params, 2);

        const newKey = params[0];
        const newValue = params[1];

        return loadStoreObj()
            .then(oldStoreObj => {
                const newStoreObj = Object.assign(oldStoreObj, {
                    [newKey]: newValue,
                });
                return saveStoreObj(newStoreObj);
            });
    },

    list: (params) => {
        validateParamsCount('list', params, 0);

        return loadStoreObj()
            .then(storeObj => {

                console.log('keys\tvalues');
                console.log('------------------');

                Object.keys(storeObj).forEach(key => {
                    console.log(`${key}\t${storeObj[key]}`);
                });
                console.log('');
            });
    },

    get: (params) => {
        validateParamsCount('get', params, 1);

        const key = params[0];

        return loadStoreObj()
            .then(storeObj => {
                console.log(storeObj[key] || 'Not found!');
            });
    },

    remove: (params) => {
        validateParamsCount('remove', params, 1);

        const key = params[0];

        return loadStoreObj()
            .then(oldStoreObj => {
                let newStoreObj = Object.assign(oldStoreObj);
                delete newStoreObj[key];
                return saveStoreObj(newStoreObj);
            });
    },

    clear: (params) => {
        validateParamsCount('clear', params, 0);
        return saveStoreObj({});
    },

}


const doProperAction = (command, params) => {

    if (!Object.keys(commandActions).includes(command)) {
        console.log('Unknown command!');
        process.exit();
    }

    const action = commandActions[command];

    //  returns a promise for the asyncronous action.
    return action(params);
}

module.exports = {
    doProperAction,
};

